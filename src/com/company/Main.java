package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        final int min = 1; // Минимальное число для диапазона
        final int max = 11; // Максимальное число для диапазона
        final int rnd = rnd(min, max);

        int input;
        int counter = 0;

        System.out.println("Игра \"Угадай число\". Компьютер загадывает число от 1 до 10. Ваша задача его угадать\n");

        if (rnd > 0 && rnd < 11) {

            do {
                System.out.println("Введите число от 1 до 10:");
                Scanner scanner = new Scanner(System.in);
                input = scanner.nextInt();
                counter++;

                if (input > rnd) {
                    System.out.println("Загаданное число меньше");
                }
                if (input < rnd) {
                    System.out.println("Загаданное число больше");
                }

            } while (input != rnd);
            System.out.println("Поздравляем. Вы угадали число " + rnd + ".\nЧисло попыток: " + counter);
        }
    }

    public static int rnd(int min, int max) {
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }
}
